// import logo from './logo.svg';
import './App.css';
import './BaiTapComponent/main.css';
import BaiTapComponent from './BaiTapComponent/BaiTapComponent';

function App() {
  return (
    <div className="App">
      <BaiTapComponent />
    </div>
  );
}

export default App;
