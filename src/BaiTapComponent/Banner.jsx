import React from 'react';

export default function Banner() {
  return (
    <div className="p-3">
      <div className="p-1 p-lg-2 bg-light rounded-3 text-left">
        <div className="m-2 m-lg-5">
          <h1 className="display-5 fw-bold">A warm welcome!</h1>
          <p className="fs-4">
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </p>
          <div className="d-flex flex-nowrap bd-highlight">
            <button className="btn btn-primary ">Call to action!</button>
          </div>
        </div>
      </div>
    </div>
  );
}
